# TurtleBot

[TurtleBot 2](http://www.turtlebot.com/) is a platform designed for research and education.
The main part is the Kobuki base, which provide basic sensors (bumper, wheel drop, etc), digital and analog inputs, digital outputs and actuators.
In addition to the Kobuki sensors, the TurtleBot 2 has a Kinect-like RGBD sensor.

 - Link: [IEEE Spectrum: TurtleBot 2](http://spectrum.ieee.org/automaton/robotics/diy/turtlebot-2-now-available-for-preorder-from-clearpath-robotics)
 - Video: [Introducing Yujin Robot's Kobuki](https://www.youtube.com/watch?v=t-KTHkbUwrU)

We have two types of TurtleBots with tree types of RGBD sensors:
 - turtle01 - turtle02: old TurtleBot 2 with Orbex Astra,
 - turtle03 - turtle07: old TurtleBot 2 with Intel RealSense R200,
 - turtle08 - turtle13: new TurtleBot 2 with Intel RealSense D435. 

Note that both the size of the image and its quality differ, Orbex Astra has 
a higher resolution and better quality (compared to Intel RealSense R200).
The minimum range which can be measured may increase with resolution (D435).

Issues with the TurtleBots
(hardware, sensor calibration, Singularity containers, ...)
can be reported at the
[RoboLab repository](https://gitlab.fel.cvut.cz/robolab/robolab/issues).

## Using Singularity Containers

Note that individual courses may have their own recommendation on what image
to use and what workspace to source or extend.

### ROS Melodic

- Start Ubuntu as Singularity container and log into its shell
  ```bash
  singularity instance start /opt/singularity/robolab/melodic ros
  singularity shell instance://ros
  ```
- Source ROS workspace
  ```bash
  source /opt/ros/robolab/setup.bash
  ```

### ROS Kinetic

- Start Ubuntu as Singularity container and log into its shell
  ```bash
  singularity instance start /opt/ros ros
  singularity shell instance://ros
  ```
- Source ROS workspace
  ```bash
  source /opt/ros/kinetic/setup.bash
  ```

- More info about Singularity at
  [DCE Wiki](https://support.dce.felk.cvut.cz/mediawiki/index.php/singularity)

## Catkin Workspace Configuration

Note that individual courses may have their own recommendation on what
workspace to source or extend.

- As the network home directories are used, you can setup your workspace on
  any robot or lab workstation.
  Lab workstation should be the preferred options due to its wired connection
  to the network filesystem and much lower latency.
  `/tmp` should be considered if even lower latency is needed.
- Be sure that you are inside the Ubuntu container (see above).

### ROS Melodic + Catkin Tools

```bash
ws=~/workspace/my_course
mkdir -p "${ws}/src"
cd "${ws}/src"
# Get and modify course-specific packages here.

cd "${ws}"
catkin init
catkin config --extend /opt/ros/robolab
catkin config --cmake-args -DCMAKE_BUILD_TYPE=Release
catkin build -c

source "${ws}/devel/setup.bash"
```

## Working with the TurtleBot

- Power up robot and onboard PC (NUC).
- **Note that switching off the robot will also cut power to the NUC.**
- After the boot, you should be able to log in
  - Using your username `ssh <username>@turtleXX` and your lab password. 
  - Or, using guest account `ssh guest@turtleXX` with password `xxx`.
- If you like to open graphical windows on your desktop use `ssh -Y ...`.
- Start singularity instance and enter the container
  (see [Using Singularity Containers](using-singularity-containers)).
- At this point it is wise to learn and start using terminal multiplexer such
  as [tmux](https://www.hamvocke.com/blog/a-quick-and-easy-guide-to-tmux/).
- Assuming that you have followed the
  [Catkin Workspace Configuration](catkin-workspace-configuration) instructions, everything should be ready.
- The robots are only visible from the local network, so in order to use
  personal notebooks use cable or wifi (ssid: `e210bot`, pass: `j6UsAC8a`).
- After your work is finished, turn off the robot and the laboratory computer
  by typing `poweroff` in terminal. It will take about 10 seconds.
  PLEASE DO NOT type `shutdown`.

### Launching the TurtleBots

Tested with new TurtleBots (turtle08 - turtle13).

To launch robot drivers:
```bash
roslaunch robolab_bringup turtlebot2.launch
``` 
Exporting hostname with `export HOSTNAME=$(hostname)` may be needed prior to roslaunch.

To launch keyboard controller:
```bash
roslaunch kobuki_keyop safe_keyop.launch
```
