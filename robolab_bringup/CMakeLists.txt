cmake_minimum_required(VERSION 2.8.3)
project(robolab_bringup)

find_package(catkin REQUIRED)

catkin_package()

## Mark other files for installation (e.g. launch and bag files, etc.)
install(DIRECTORY
    launch/
    DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
)

install(DIRECTORY
    config/
    DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/config
)
